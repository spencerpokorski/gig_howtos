# MBOX Configuration Guide

## Quick and Dirty. Possibly unsafe, use at your own risk, don't blame me if the Production Manager throws you off the stage when you're not looking.

### Step 1 - User management

If the username has spaces in it, I change it to something without, or just make a new user account. Making a new user account means you don't have to reboot the computer. Make sure you configure the new user as an admin.

Whatever user account you use, you should give it a password. Don't be a dick and keep this to yourself. If you get oil spotted someone's going to need access, so maybe write it in fuckoff huge letters on some gaff tape someplace where someone will find it whilst panicking. I usually set user mbox, password mbox. That's easy to remember.

### Step 2 - Configure IP address in a sane, ArtNet-appropriate manner.

You don't have to use ArtNet, but it's the default, and really works just fine if you're not saturating your link. Remember, 4 universes is theoretically the maximum you should have on one ArtNet line, so nobody will feel sympathy if you have problems over that. But of course, more than 4 universes has worked perfectly fine for many people and shouldn't really be a problem.

Example:

  * Server 1
    * IP Address: 2.0.0.101
    * Subnet Mask: 255.0.0.0

  * Server 2
    * IP Address: 2.0.0.102
    * Subnet Mask: 255.0.0.0

The subnet mask of 255.0.0.0 means you can see all other items in the 2.x.x.x address range (a.k.a., the 2.0.0.0/8 range). 

*IMPORTANT* - The trashcans have 2 Ethernet NICs. Make sure you're configuring the correct one, which should be the only one with a cable connected. You're plugged into a switch, too....right?

### Step 3 - verify the machines can actually talk to one another.

Open Terminal.app (I use spotlight for this - ⌘ + Space).

From one machine, ping the other machine.

Example: 

`ping 2.0.0.101` _(from the second machine, with the 2.0.0.102 address)_

Your response should look something like this:

```sh
sp at sp-mbp17 in ~
$ ping nsa.gov
PING nsa.gov (23.197.244.75): 56 data bytes
64 bytes from 23.197.244.75: icmp_seq=0 ttl=50 time=42.731 ms
64 bytes from 23.197.244.75: icmp_seq=1 ttl=50 time=45.481 ms
^C
--- nsa.gov ping statistics ---
2 packets transmitted, 2 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 42.731/44.106/45.481/1.375 ms
```

If you don't see reply saying `XX bytes from 2.0.0.101` or something similar, that means the machines don't want to talk. You've either got the wrong NIC configured to the correct IP address, there is a conflict with subnet masks, a cable may be bad, switch doesn't work, etc. Double check your settings, try new cables, make sure your switch doesnt have shitty management settings (the programmer would see that problem, too).

### Step 4 - Set up your management environment

Some people will say use Apple Remote Desktop to manage the mbox, which is fine, but your mbox remote machine should be a rental machine which doesn't go on the internet or serve any other functions.
Apple Remote Desktop is installed from the App Store, requiring an icloud account and your willingness to drop $80 on a piece of software designed strictly to accomodate laziness.

Do this, instead:

Open System Preferences on the Mac again, and go to "Sharing"

Enable Screen Sharing and File Sharing, and restrict it to your user "mbox"

I live in the command line, even on my Macs, so I would also enable "Remote Login" which would allow for the use of SFTP protocol and ssh. SFTP is a secure file transfer protocol, and I prefer it as it gives me real feedback. For whatever reason, the "Network" tab in Finder doesn't always populate and that can suck.

If you're interested in SFTP, you can download a GUI client called "CyberDuck," or you can learn how to use it from the command line. Open Terminal.app and type in "man sftp".  This is the built in user's manual present on every Unix-like computer system (Mac, Linux, FreeBSD...).  j key moves down, k key moves up, space advances a page, and q key quits. Most all command line functions have a man page, which will provide you with information on how to use that command.

### Step 5 - Test screen sharing

The screen sharing feature of MacOS is actually just VNC. To access it, you can open "Screen Sharing.app" or you could just type in "vnc://2.0.0.102" in the spotlight view. You will be asked for credentials. If the world is happy, you should see the local desktop on the mbox.

### Step 6 - Get the latest version of Mbox, and configure the Daemon

When I wrote this, that was v 4.2.2

I believe Remote and Studio will have the same version number

You can find this at PRG's website. Using Google, of course.

After you've installed everything, go ahead and start Mbox Studio. If Spotlight doesn't find it, it lives in the Applications folder.

When you start it, another application called "MboxDaemon" starts. In a simple setup, the only thing you care about in this window is the Network Interface field. Chose the correct NIC.

### Step 7 - Jump into Mbox Studio and start configuring shit

If you can't see the Mbox Studio window when you switch to it, but only a blank canvas, click "Window" at the top and select "Mbox Studio", or just click ⌘ + \` until the screen comes up.


#### Patch

Your first tab is Patch. This is where you control the ArtNet settings. I usually use the profile Studio v4.x and 1 output master. Remember, ArtNet starts at universe 0...

This Patch tab also includes a rudimentary ArtNet monitor which is great for figuring out issues with console profiles, which can suck because most Mbox programmers make their own anyways.

#### Preferences

Most of these are pretty reasonable as default values. Here are some options to keep an eye on:

##### Network->Management
Set this to the correct NIC

##### Network->Sync
Set this to the same NIC

##### Core->Show White Square
By default, when you load a layer and give it intensity (opacity, for the video snobs), if it doesn't have any content in the current Folder/File, it will display a small white square.

This functionality is very much a programmer's preference, and in some situations can be useful to see you've overshot your encoder spin. Some programmers will fucking hate it and swear you've broken the machine, however, so know that by unchecking this box that feature is disabled.

##### Output->Start in Fullscreen
When you restart the mbox, it's going to start in window mode. Check this box so that when the software starts up, it comes up fullscreen.

There are commands to make mbox go full screen from within Remote, but if you've had a crash and switched to your backup, it's nice when you can restart the software/machine and watch it come up fullscreen all on its own. By default, this feature is off. I say enable it. When you're local, ⌘ + f toggles between fullscreen and windowed mode.

##### Misc->MboxTime Source
This is where you choose between the oldschool Network (don't ask) mode or the Audio mode. Choose Audio if you're listening to LTC.

##### Audio->Audio Input Source
Here, select the correct input device for your timecode signal.

##### Audio->Audio Input Channel
Select the correct channel carrying LTC data.

##### Audio->Log TC Info
If you're experiencing problems with Timecode, you can enable this and it will log discrepancies or hiccups in timecode.

##### Audio->Unlimited LTC Hours
This isn't quite kosher, but if the tracks are coming at you with values higher than 23:59:59:29, this will let you go up to 39:59:59:29

##### Pixel Mapping->PixMap Enable
Check this box to get some sweet pixel mapping action going.

#### Outputs

This is where you add outputs. It's pretty self explanatory, but I usually only use one Output Master. Do uncheck "Auto Resize." That setting means when you drag a mix off the canvas it will resize the entire surface. That's a lame option to have when we're dealing with pixel-accuracy.

If your Mac doesn't hate you for calling it a trash can, your outputs should remain assigned to the correct physical output each time you boot up. If that doesn't happen, you should change that here, not in your router.

Again, I use one output master because I just don't care about having separate ones. To do that, there's a dropdown right beneath where you choose the display.This is where you add outputs. It's pretty self explanatory, but I usually only use one Output Master. Do uncheck "Auto Resize." That setting means when you drag a mix off the canvas it will resize the entire surface. That's a lame option to have when we're dealing with pixel-accuracy.

If your Mac doesn't hate you for calling it a trash can, your outputs should remain assigned to the correct physical output each time. If that doesn't happen, you should change that here, not in your router.

Again, I use one output master because I just don't care about having separate ones. To assign output masters, there's a dropdown right beneath where you choose the display.

#### Mixes

When programming an Mbox, your layer's mix is arbitrary. By default, each of your outputs is a mix - 001, or 002, etc etc. The mixes you add start at 33, and it's a good idea to keep that as that is the number used in the DMX mix select channel. Some fixture profiles expect "Mix 1" is going to be DMX 33, etc.

Again, make sure the auto-resize feature is off.

A *really* cool feature of mbox is that you can overlap mixes. Say you have a really wide hi-res screen you want to display video in 3 different squares, but sometimes you want to display video across the entire screen. You'd create 3 mixes which were each 1/3 of the screen, and a 4th mix which is the full width. Order front-to-back is determined by layer order.

### Step 8 - Playing with Mbox Remote
So, now you're pretty much set up locally on each machine. Put the damnn thing in full screen and close out of "Screen Sharing"

On the left side you should see an empty column with a message "click Edit Servers to begin." Do that.

If everything is happy with the networks (did you remember to change the Network Adaptor in MboxDaemon?), you should see your servers in the bottom list.

Just as it says, double click a server to add it to your managed list. Do that for both.

They will appear in the "Servers" list, showing a rough preview of their output canvas, current IP, current LTC value, and whether they're windowed or full screen.

#### Status Tab
Here you can drag the servers to make a sexy status window showing what's going on with each Layer and Output. These things don't update very fast to keep the Network overhead low, but it can be a nice aid at times.

Play with these options and you'll come up with some layouts you like, or you may end up never using it at all.

#### Content Tab
WHERE THE MAGIC HAPPENS! 

In that far left column, which is your list of servers, you can right click (or Option+Click if you're used to Macs with only one mouse button) and the context menu appears. Here, you can remove a server, and you can select it's syncing preferences.

Whichever server is going to be your master (the one you upload content to), go ahead and right click it, selecting "Master of a"

Your other servers, who will be sucking content down from the master, need to be set up as "Syncing -> Collection -> a"

Once you have established a server as master you should see all of your folders appear in the next column, labeled "Media"

##### *FOLDERS NOTE*  
Mbox media lives under the directory `/Mbox/media/`  
Finder will do everything it can to keep you from seeing the `/` directory on your Mac, so go ahead and use the "Go to Folder Command" in Finder, or type in `/Mbox/media` directly into spotlight.  
Each folder must start with a 3 digit number from 000 thru 255, followed by a period -> `.`  
Each file must have the same prefix.  
Mbox will whine at you about duplicate folder numbers or duplicate file numbers within each folder.  

Example:  

```
/Mbox/media/001.greenTrees/001.Oak.mov
/Mbox/media/001.greenTrees/002.Pine.mov
/Mbox/media/001.greenTrees/003.Cannabis.mov
/Mbox/media/002.flames/001.fireplace.mov
/Mbox/media/002.flames/002.Bic_Lighter.mov
/Mbox/media/002.flames/003.the_donald_trump_dumpster_fire.mov
```

When you select a "member of a" in the left (your backup server, or additional severs, whatever), the "content" view will show blacked out folders that it sees on the "master" server, but which need to be synced.

If you click the Recycle icon next to each of the folders you don't have, they'll be primed for syncing.

Back on the master, you'll see a highlighted Recycle symbol next to each folder which will sync. Click that and then the content will sync!

Mbox Remote will show you the progress, current file, eta, and so on.

#### Pixel Mapping

I've not used pixel mapping on mbox v4, but it looks pretty straightforward...pay attention to the tab at the bottom of the screen that says "context 1"

#### Preferences, Outputs, Mixes, Image Remap...

These are the same as what you saw in the local Mbox Studio application on each server. Once you're dialed in, you shouldn't expect to make a lot of changes here, but if you want, you can jump into this and do the mix and output configuration for both machines at the same time...ish.

### That's honestly, actually, fucking it. It's easy and fun and works pretty damn well. Remember, content wants to be ProRes 422, and the capabilities are only as good as the Mac it's run on.
